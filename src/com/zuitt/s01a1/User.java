package com.zuitt.s01a1;

public class User {
    public static void main(String[] args) {
        String firstName = "Charles";
        String lastName = "Babbage";
        double englishGrade = 92.10;
        double mathGrade = 85.00;
        double scienceGrade = 87.50;
        double ave = (englishGrade + mathGrade + scienceGrade) / 3;

        System.out.println("The average grade for " + firstName + " " + lastName + " is " + ave);

    }
}
